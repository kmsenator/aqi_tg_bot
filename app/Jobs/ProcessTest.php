<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessTest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $datas;

    public function __construct($datas)
    {
        $this->datas = $datas;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
//        dd($this->datas);
        $baseUrl = env("TELEGRAM_API_URL");
        $token = env("TELEGRAM_BOT_TOKEN");

        $client = new Client(
            ['base_uri' => $baseUrl . 'bot' . $token . '/']
        );

        $client->request('GET', 'sendMessage',
            [
                'query' => [
                    'chat_id' => $this->datas,
                    'text' => 'wwtext',
                    'reply_markup' => null
                ]
            ]);
    }
}
