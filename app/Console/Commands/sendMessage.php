<?php

namespace App\Console\Commands;

use App\Services\MessageService;
use Illuminate\Console\Command;

class sendMessage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:message';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $baseUrl;
    private $token;
    private $client;

    public function __construct()
    {
        parent::__construct();
    }



    public function handle()
    {
        $message = new MessageService();
        $message->sendMessageByUsers();

        return 0;
    }
}
