<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/phpinfo', function () {
    return phpinfo();
});


Route::get('/palindrom', [\App\Http\Controllers\WebController::class, 'palindrom'])->middleware('ip');


//Route::get('/ip', function () {
//    return "ваш ip: ".$_SERVER['REMOTE_ADDR'];
//});
Route::get('/ip', [\App\Http\Controllers\WebController::class, 'validateIP']);


Route::get('/tgusers', [\App\Http\Controllers\WebController::class, 'getTGUsers']);
