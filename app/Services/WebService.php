<?php


namespace App\Services;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use PHPHtmlParser\Dom;
use PHPHtmlParser\Exceptions\ChildNotFoundException;
use PHPHtmlParser\Exceptions\CircularException;
use PHPHtmlParser\Exceptions\ContentLengthException;
use PHPHtmlParser\Exceptions\LogicalException;
use PHPHtmlParser\Exceptions\NotLoadedException;
use PHPHtmlParser\Exceptions\StrictException;
use Illuminate\Support\Facades\DB;

class WebService
{

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function checkCity($text) {
        $atext = mb_strtolower(preg_replace('/[^a-z]/ui', '', $text));
        $this->client = new Client(
            ['base_uri' => 'https://air.plumelabs.com/']
        );
        $response = $this->client->request('GET', 'api/v1/autocomplete',
            [
                'query' => ['input' => $atext, 'locale' => 'en']
            ]);
//        dd($atext);
        if ($response->getStatusCode() === 200) {
            return $response->getBody()->getContents();
        }
        return null;
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getInfo($path) {
        $this->client = new Client(
            ['base_uri' => 'https://air.plumelabs.com/']
        );
        $response = $this->client->request('GET', $path,
            [

            ]);
        if ($response->getStatusCode() === 200) {
//            dd($path);
            return $response->getBody()->getContents();
        }
        return null;
    }

    public function parseInfo($html)
    {
        $arr_info = [];
        $arr_info['data'] = '';
        $arr_info['aqi'] = 0;

        try {


            $dom = new Dom;
            $dom->loadStr($html);

            $titles = $dom->find('div.report-section');
            $title = trim($titles->find('h1',0)->text) . "\n" . trim($titles->find('h2',0)->text);

            $weathers = $dom->find("div.report__weather");
            $weather = trim($weathers->find('span', 0)->text) . "°C ☁️   " . trim($weathers->find('span', 1)->text)."UV ☀";

            $pi_level = ucfirst(trim($dom->find("div.report__pi-level")->text));
            $aqi = trim($dom->find("div.report__pi-number")->find('span', 0)->text);
            $arr_info['aqi'] = $aqi;

            $pollutants_array = ['pollution-level-icon--1', 'pollution-level-icon--2', 'pollution-level-icon--3', 'pollution-level-icon'];
            $activity_type11 = trim(str_replace($pollutants_array, '', $dom->find("li.activity__header", 0)->find("span.activity__icon")->find('span', 0)->getAttribute('class')));
            $activity_type12 = trim(str_replace($pollutants_array, '', $dom->find("li.activity__header", 0)->find("span.activity__icon")->find('span', 1)->getAttribute('class')));
            $activity_type13 = trim(str_replace($pollutants_array, '', $dom->find("li.activity__header", 0)->find("span.activity__icon")->find('span', 2)->getAttribute('class')));

            $activity_type21 = trim(str_replace($pollutants_array, '', $dom->find("li.activity__header", 1)->find("span.activity__icon")->find('span', 0)->getAttribute('class')));
            $activity_type22 = trim(str_replace($pollutants_array, '', $dom->find("li.activity__header", 1)->find("span.activity__icon")->find('span', 1)->getAttribute('class')));
            $activity_type23 = trim(str_replace($pollutants_array, '', $dom->find("li.activity__header", 1)->find("span.activity__icon")->find('span', 2)->getAttribute('class')));

            $activity_type31 = trim(str_replace($pollutants_array, '', $dom->find("li.activity__header", 2)->find("span.activity__icon")->find('span', 0)->getAttribute('class')));
            $activity_type32 = trim(str_replace($pollutants_array, '', $dom->find("li.activity__header", 2)->find("span.activity__icon")->find('span', 1)->getAttribute('class')));
            $activity_type33 = trim(str_replace($pollutants_array, '', $dom->find("li.activity__header", 2)->find("span.activity__icon")->find('span', 2)->getAttribute('class')));

//            dd($activity_type11,$activity_type12,$activity_type13,$activity_type21,$activity_type22,$activity_type23,$activity_type31,$activity_type32,$activity_type33);

            if ($activity_type11 == "--active") $status1 = "✅";
            if ($activity_type21 == "--active") $status2 = "✅";
            if ($activity_type31 == "--active") $status3 = "✅";

            if ($activity_type12 == "--active") $status1 = "❕";
            if ($activity_type22 == "--active") $status2 = "❕";
            if ($activity_type32 == "--active") $status3 = "❕";

            if ($activity_type13 == "--active") $status1 = "❌";
            if ($activity_type23 == "--active") $status2 = "❌";
            if ($activity_type33 == "--active") $status3 = "❌";

//dd($status1,$status2, $status3);
            $advice = trim($dom->find("div.activity__advice")->text);

            for ($i = 0; $i < 8; $i++) {
                $pollutants[$i] = trim($dom->find("li.pollutant-column", $i)->find('div', 0)->text);
            }
//            dd($pollutants);
            $pollutants_table = "<pre>+-------------------------------+
|               AQI             |
+-------------------------------+\n".
"|".str_pad($pollutants[0], 7, ' ', STR_PAD_BOTH).
"|".str_pad($pollutants[1], 8, ' ', STR_PAD_BOTH).
"|".str_pad($pollutants[2], 7, ' ', STR_PAD_BOTH).
"|".str_pad($pollutants[3], 6, ' ', STR_PAD_BOTH)."|\n".
"+-------------------------------+
| PM2.5 |  PM10  |  NO2  |  O3  |
+-------------------------------+
|              µg/m³            |
+-------------------------------+\n".
"|".str_pad($pollutants[4], 7, ' ', STR_PAD_BOTH).
"|".str_pad($pollutants[5], 8, ' ', STR_PAD_BOTH).
"|".str_pad($pollutants[6], 7, ' ', STR_PAD_BOTH).
"|".str_pad($pollutants[7], 6, ' ', STR_PAD_BOTH)."|\n".
"+-------------------------------+
| PM2.5 |  PM10  |  NO2  |  O3  |
+-------------------------------+</pre>";
//            dd($pollutants_table);

            $data = "<b>".$title."</b>" . "\n\n" . $weather . "\n\n" . "<b>".$aqi." AQI (".$pi_level.")</b>"
                . "\n\n". "" . $status1 ." OUTDOOR SPORTS  " . $status2 . " BRING BABY OUT  " . $status3 . " EATING OUTSIDE" . ""
                . "\n\n". "<b>".$advice."</b>" . "\n\n<b>MAIN POLLUTANTS</b>\n" . $pollutants_table;

            $arr_info['data'] = $data;

            return $arr_info;

        } catch (ChildNotFoundException | CircularException | ContentLengthException | LogicalException | NotLoadedException | StrictException $e) {
            $arr_info['data'] = "Something went wrong. Try again.";
            return $arr_info;
        }
    }

    public function checkPalindrom($text): bool
    {
        $text = $text->get('query');

        //$atext = mb_str_split(mb_strtolower(str_replace(array("?","!",",",";",".","—","-",":"," "), "", text)));
        $atext = mb_str_split(mb_strtolower(preg_replace('/[^a-zа-яё\d]/ui', '', $text))); //удаление символов через регулярное выражение, цифры оставлены, но можно убрать (удалив \d)
        $rtext = array_reverse($atext);
        if ($atext == $rtext) {
            return true;
        } else {
            return false;
        }
    }

    public function palindrom(Request $request)
    {
        $checker = $this->checkPalindrom($request->query);
        if ($checker) { $content = "Палиндром"; }
        else { $content = "Не палиндром"; }
        echo <<<EOF
        Проверяем строку "{$request->get('query')}" на палиндром<br>
        Ответ: $content
        EOF;
    }

    public function botPalindrom($text): bool
    {
        $atext = mb_str_split(mb_strtolower(preg_replace('/[^a-zа-яё\d]/ui', '', $text))); //удаление символов через регулярное выражение, цифры оставлены, но можно убрать (удалив \d)
        $rtext = array_reverse($atext);
        if ($atext == $rtext) {
            return true;
        } else {
            return false;
        }
    }

    public function validateIP()
    {
        $white_ip = true;
        $ip_addr = $_SERVER['REMOTE_ADDR'];
        $ip_addr2long = ip2long($ip_addr);
        if (($ip_addr2long >= 167772160 && $ip_addr2long <= 184549375) || ($ip_addr2long >= 2886729728 && $ip_addr2long <= 2887778303) || ($ip_addr2long >= 3232235520 && $ip_addr2long <= 3232301055)) {
            $white_ip = false;
        }

        echo "ВАШ IP: $ip_addr<br>IP: ";
        if ($white_ip) {
            echo "белый";
        } else {
            echo "серый";
        }
    }

    public function getUsers() {
        return DB::select('select * from users');
    }
}
