<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Palindrom</title>


    </head>
    <body>
        <p><b>Проверяем строку на палиндром</b></p>
        <h2>{{ $query }}</h2>
        <p><b>Ответ: {{ $answer }}</b></p>
        @error('query')
            <div style="color: #8b0000">Ошибка: {{ $message }}</div>
        @enderror
    </body>
</html>
