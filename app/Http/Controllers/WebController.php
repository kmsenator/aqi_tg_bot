<?php

namespace App\Http\Controllers;

use App\Services\WebService;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class WebController extends Controller
{
    private $webService;

    public function __construct(WebService $webService)
    {
        $this->webService = $webService;
    }

    public function palindrom(Request $request)
    {
        try {
            $this->validate($request, [
                'query' => 'required'
            ], $messages = [
                'query.required' => 'Необходимо указать текст в параметре query'
            ]);

            $this->webService->palindrom($request);

        } catch (ValidationException $e) {
            echo $messages['query.required'];
        }
    }

    public function array_to_html($data) {
        $report = "";

        if (count($data) > 0) {

            $report .= "<table border='1'>";
            $report .= sprintf("<tr><th>%s</th></tr>", join("</th><th>", array_keys($data[0])));

            foreach ($data as $row) {

                $report .= "<tr>";

                foreach ($row as $column) {
                    $report .= "<td>$column</td>";
                }
                $report .= "</tr>";
            }
            $report .= "</table>";
        } else {
            $report = "No data";
        }

        return $report;
    }

    public function validateIP()
    {
        $this->webService->validateIP();
    }

    public function getTGUsers()
    {
        $users = $this->webService->getUsers();

        if (!empty($users)) {
            $new_users = [];
            foreach ($users as $user) {
//                dd(get_object_vars($user));
                $new_users[] = get_object_vars($user);

            }
            echo  $this->array_to_html($new_users);
        }

    }

}
