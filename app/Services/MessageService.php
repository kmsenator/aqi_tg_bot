<?php


namespace App\Services;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use PHPHtmlParser\Exceptions\ChildNotFoundException;
use PHPHtmlParser\Exceptions\CircularException;
use PHPHtmlParser\Exceptions\ContentLengthException;
use PHPHtmlParser\Exceptions\LogicalException;
use PHPHtmlParser\Exceptions\NotLoadedException;
use PHPHtmlParser\Exceptions\StrictException;
use Illuminate\Support\Facades\DB;

class MessageService
{
    private $baseUrl;
    private $token;
    private $client;

    public function __construct()
    {
        $this->baseUrl = env("TELEGRAM_API_URL");
        $this->token = env("TELEGRAM_BOT_TOKEN");

        $this->client = new Client(
            ['base_uri' => $this->baseUrl . 'bot' . $this->token . '/']
        );
//        dd($this->baseUrl . 'bot' . $this->token . '/');
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */

    public function isRussian($text) {
        return preg_match('/[А-Яа-яЁё]/u', $text);
    }


    public function getData($text): array
    {
        $arr = [];
        $arr['check'] = false;
        $arr['data'] = '';
        $arr['path'] = '';
        $arr['aqi'] = 0;
//        dd($this->isRussian($text));
        if ($this->isRussian($text)) {
            $data = "Ой-ой! Я не знаю русский, как мой хозяин. Пожалуйста, пиши города на английском.\nИ, да, я рассказываю о погоде и чистоте воздуха в набираемом городе.";
            $arr['check'] = false;
        } elseif ($text == "/start") {
            $data = "Greetings, health conscious person. Do you want to know about the air quality in your city?\nJust send the city name in the reply message.";
            $arr['check'] = false;
        } else if (mb_substr($text,0,1) == "/") {
            $data = "I thick that `$text` is not city or town.";
            $arr['check'] = false;
        } else {
            //dd($messages);
            $webservice = new WebService();

            if (!is_null($webservice->checkCity($text))) {
                $arr['check'] = true;
                //                        dd($webservice->checkCity($text));
                $answer_from_api = json_decode($webservice->checkCity($text), true);
                if (count($answer_from_api) && count($answer_from_api['result'])) {
                    $url = $answer_from_api['result'][0]['href'];
                    $arr['path'] = $url;
                    $answer = $webservice->getInfo($url);
                    try {
                        $info = $webservice->parseInfo($answer);
                        $data = $info['data'];
                        $arr['aqi'] = $info['aqi'];
                    } catch (ChildNotFoundException | CircularException | ContentLengthException | LogicalException | NotLoadedException | StrictException $e) {
                        $data = "Something went wrong. Try again.";
                        $arr['check'] = false;
                    }
                } else {
                    $data = "Unfortunately, it is impossible to get information about this city. Try typing a different city.";
                    $arr['check'] = false;
                }
            } else {
                $data = "Unfortunately, it is impossible to get information about this city. Try typing a different city.";
                $arr['check'] = false;
            }
        }
        $arr['data'] = $data;

        return $arr;
    }

    public function getMessages() {
        try {

            $response = $this->client->request('GET', 'getUpdates',
                [
                    'query' => [
                        'offset' => -1
                    ]
                ]);


            if ($response->getStatusCode() === 200) {
                $messages = json_decode($response->getBody()->getContents(), true);
//                dd($messages);

                foreach ($messages['result'] as $result) {
                    if (isset($result['update_id']) && is_int($result['update_id'])) {
                        $updateId = $result['update_id'];
                        $message = DB::select('select * from messages where update_id = :uid', ['uid' => $updateId]);

                        if (empty($message)) {
                            if (isset($result['message']['text'])) {
                                $text = ucfirst(trim($result['message']['text']));
                                $userId = $result['message']['from']['id'];
                                $chatId = $result['message']['chat']['id'];
//                                dd($userId);
                                if (empty($chatId)) $chatId = $userId;
                                $messageId = $result['message']['message_id'];
                                $status = false;
                                if (isset($result['message']['from']['username'])) {
                                    $username = $result['message']['from']['username'];
                                } else $username = '';
                                if (isset($result['message']['from']['first_name'])) {
                                    $first_name = $result['message']['from']['first_name'];
                                } else $first_name = '';
                                if (isset($result['message']['from']['last_name'])) {
                                    $last_name = $result['message']['from']['last_name'];
                                } else $last_name = '';

                                $id = DB::insert('insert into messages (user_id, update_id, message_id, chat_id, text, status) values (?, ?, ?, ?, ?, ?)',
                                    [$userId, $updateId, $messageId, $chatId, $text, $status]);

                                $arr_data = $this->getData($text);
                                $data = $arr_data['data'];
                                $prefix = '';
                                $path = $arr_data['path'];

                                if ($arr_data['check']) {

                                    $prefix = "<b>Good city - {$text}. I save one as the favorite.\nYou will be informed about the level of pollution in it.</b>\n\n";

                                    $users = DB::select('select * from users where user_id = :id', ['id' => $userId]);
                                    if (empty($users)) {
                                        $id = DB::insert('insert into users (user_id, username, first_name, last_name, city, path, status) values (?, ?, ?, ?, ?, ?, ?)',
                                            [$userId, $username, $first_name, $last_name, $text, $path, 1]);
                                    } else {
                                        if (mb_strtolower($users[0]->city) == mb_strtolower($text)) $prefix = '';
                                        else
                                        $affected = DB::update(
                                            'update users set username = ?, first_name = ?, last_name = ?, city = ?, path = ? where user_id = ?',
                                            [$username, $first_name, $last_name, $text, $path, $userId]
                                        );
                                    }
                                }
                                if ($text == "/stop") {
                                    $affected = DB::update(
                                        'update users set status = ? where user_id = ?',
                                        [0, $userId]
                                    );
                                    $data = "I have stopped informing about your favorite city :)";
                                }
                                if ($text == "/subscribe") {
                                    $prefix = '';
                                    $users = DB::select('select * from users where user_id = :id', ['id' => $userId]);
                                    if (!empty($users)) {
//                                        dd($users[0]->city);
                                        if (!is_null($users[0]->city) && $users[0]->city != '') {

                                            $affected = DB::update(
                                                'update users set status = ? where user_id = ?',
                                                [1, $userId]
                                            );
                                            $data = "I have started informing about your favorite city ;)";
                                        } else {
                                            $data = "I don't know your favorite city. Send me one ;)";
                                        }

                                    } else {
                                        $data = "Hm. You need to type any city ;)";
                                    }


                                }
//                                dd($text, $userId);206175488
                                if (($text == "/users") && ($userId == 206175488)) {

                                    $prefix = '';
                                    $users = DB::select('select * from users');
                                    if (!empty($users)) {
                                        $data = "";
                                        $new_users = [];
                                        foreach ($users as $user) {

                                            $new_users[] = get_object_vars($user);

                                        }
                                        $report = "";

                                        if (count($new_users) > 0) {

                                            $report .= sprintf("| %s |\n", join("|", array_keys($new_users[0])));

                                            foreach ($new_users as $row) {

                                                $report .= "|";

                                                foreach ($row as $column) {
                                                    $report .= " $column |";
                                                }
                                                $report .= "\n";
                                            }

                                        } else {
                                            $report = "No data";



                                        }
                                        $data = $report;
                                    } else {
                                        $prefix = '';
                                        $data = "Wow! You are very lucky!";
                                    }
                                }
                                if ($text == "100") {
                                    $prefix = '';
                                    $data = "Хозяин готов принять $100 от вас!";
                                }
                                if ($text == "42") {
                                    $prefix = '';
                                    $data = "Главный вопрос жизни, вселенной и всего такого\n\nThe Ultimate Question of Life, the Universe, and Everything";
                                }

                                if ($id) { $state_send = $this->sendMessage($chatId, $prefix . $data); }
                                if ($state_send) {
                                    $affected = DB::update(
                                        'update messages set status = true where update_id = ?',
                                        [$updateId]
                                    );
//                                    dd($affected);
                                }

                            }
                        } else {
//                            dd($message[0]->status);
                            if (!$message[0]->status) {
                                $send_text = $this->getData($message[0]->text);
                                $state_send = $this->sendMessage($message[0]->chat_id, $send_text['data']);
                                if ($state_send) {
                                    $affected = DB::update(
                                        'update messages set status = true where update_id = ?',
                                        [$updateId]
                                    );
                                    //                                    dd($affected);
                                }
                            }
                        }
                    }
                }
            }
        } catch (GuzzleException $e) {
        }


    }

    public function sendMessage($chatId, $text): bool
    {
        try {
            $response = $this->client->request('GET', 'sendMessage',
                [
                    'query' => [
                        'chat_id' => $chatId,
                        'text' => $text,
                        'reply_markup' => null,
                        'parse_mode' => 'HTML'
                    ]
                ]);
            if ($response->getStatusCode() === 200) {
                $state = json_decode($response->getBody()->getContents(), true);
                if (isset($state["ok"]) && $state["ok"]) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (GuzzleException $e) {
        }
    }

    public function sendMessageByUsers()
    {
        $POL = 50;
        try {
            $users = DB::select('select * from users where status = 1');
//            dd($users);
            if (!empty($users))
                foreach ($users as $user) {
                    $data = $this->getData($user->city);
//                    dd($text);
                    if ($data['aqi'] > $POL) {
                        $response = $this->client->request('GET', 'sendMessage',
                            [
                                'query' => [
                                    'chat_id' => $user->user_id,
                                    'text' => "⚡️⚡️⚡️⚡️ <b>Attention Poor air quality</b> ⚡️⚡️⚡️⚡️\n\n" . $data['data'],
                                    'reply_markup' => null,
                                    'parse_mode' => 'HTML'
                                ]
                            ]);
                        if ($response->getStatusCode() === 200) {
                            $state = json_decode($response->getBody()->getContents(), true);
                            if (isset($state["ok"]) && $state["ok"]) {
                                return true;
                            } else {
                                return false;
                            }
                        }
                    }
                }
            return false;
        } catch (GuzzleException $e) {
        }

    }

}
